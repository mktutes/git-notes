# Git Reference Materials

## For Practice / App Downloads
1. [Github](https://github.com/)
1. [Bitbucket](https://bitbucket.org/)
1. [Git Official Site](https://git-scm.com/)

## Books / Tutorials
1. [Pro Git eBook](https://git-scm.com/book/en/v2)
1. [Learn Git with Bitbucket Cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)
1. [Official Git Tutorials](https://git-scm.com/docs/gittutorial)
1. [Curious Git](https://matthew-brett.github.io/curious-git/)
1. [Visualize Git Commamds](http://onlywei.github.io/explain-git-with-d3/#) **NEW**
1. [Visual Git Reference](https://marklodato.github.io/visual-git-guide/index-en.html) **NEW**
1. [Git Lecture from MIT](https://missing.csail.mit.edu/2020/version-control/) **NEW**

## Courses
1. [Version Contro with Git: Coursera](https://www.coursera.org/learn/version-control-with-git/)

## Videos
1. [Git tutorial for Beginners - 1hr](https://www.youtube.com/watch?v=8JJ101D3knE)
1. [Git and GitHub for Beginners - 1hr](https://www.youtube.com/watch?v=RGOj5yH7evk)
1. [How Git Branches Work](https://www.freecodecamp.org/news/how-git-branches-work/)

## Git Internals
1. [Chapter 10 @ Progit ebook](https://git-scm.com/book/en/v2/Git-Internals-Git-Objects)
1. [Git Internals Youtube Video](https://www.youtube.com/watch?v=nHkLxts9Mu4)
1. [Git SHA from Gitlab](https://www.youtube.com/watch?v=P6jD966jzlk)

## Useful Links
1. [Exclude files using .gitignore](https://www.atlassian.com/git/tutorials/saving-changes/gitignore)
1. [Git Branches](https://www.w3docs.com/learn-git/git-branch.html)
1. [Git Branches @ Altassian](https://www.atlassian.com/git/tutorials/using-branches)

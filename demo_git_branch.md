## Git Branches Demo

This file has just the commands, results and minimal info

## create a remote repo for branch demo

```
$ git clone https://mktutes@bitbucket.org/mktutes/demo-branch.git
Cloning into 'demo-branch'...
remote: Enumerating objects: 3, done.
remote: Counting objects: 100% (3/3), done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), done.
```

## list existing branches

```
# master: default branch
# * master: * in front of branch name means its the active branch
$ cd demo-branch/
$ git branch
* master
```

## Add a file and commit / sync with remote

```
# create an file
$ vi arith.py

# display the contents of arith.py
$ cat arith.py
"""
"""

# track, commit and push changes 
$ git add .
$ git commit -m "arith:master - scaffolding"
[master 0b1495a] arith:master - scaffolding
 1 file changed, 2 insertions(+)
 create mode 100644 arith.py

$ git push -u origin master
Counting objects: 3, done.
Delta compression using up to 12 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 309 bytes | 309.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/mktutes/demo-branch.git
   4d50f36..0b1495a  master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
```

## Create branches

```
$ git branch a-plus

# list branches
# master is stil active branch
$ git branch
  a-plus
* master

# create more branches
$ git branch a-subtract
$ git branch a-prod
$ git branch a-div
$ git branch a-mod
$ git branch a-add_sub
$ git branch a-mul_div_mod
$ git branch a-min
$ git branch
  a-add_sub
  a-div
  a-min
  a-mod
  a-mul_div_mod
  a-plus
  a-prod
  a-subtract
* master
```

## delete a branch
```
$ git branch -d a-min
Deleted branch a-min (was 0b1495a).

# Delete branch with changes that are not commited
# we are discarding the changes
# for recovery, lookup 'git reflog' command 
$ git branch -D a-min
```

## rename a branch 
```
$ git branch -m a-subtract a-minus
$ git branch
  a-add_sub
  a-div
  a-minus
  a-mod
  a-mul_div_mod
  a-plus
  a-prod
* master

# renaming current branch
$ git branch -m a-minus a-subtract

# make 'a-subtract' as current branch
$ git checkout a-subtract
Switched to branch 'a-subtract'
$ git branch
...
* a-subtract
  master

$ git branch -m a-minus
$ git branch
...
* a-minus
...
  master
```

## Commit History

```
$ git log --oneline
0b1495a (HEAD -> a-minus, origin/master, origin/HEAD, master, a-prod, a-plus, a-mul_div_mod, a-mod, a-div, a-add_sub) arith:master - scaffolding
4d50f36 Initial commit
$ git log --oneline --all
0b1495a (HEAD -> a-minus, origin/master, origin/HEAD, master, a-prod, a-plus, a-mul_div_mod, a-mod, a-div, a-add_sub) arith:master - scaffolding
4d50f36 Initial commit
```

## checkout a branch (active) 

```
$ git checkout a-minus
$ git branch
  a-add_sub
  a-div
* a-minus
  a-mod
  a-mul_div_mod
  a-plus
  a-prod
  master
```

### change a-minus branch

```
$ cat arith.py
"""
"""

$ vi arith.py 

$ cat arith.py 
"""
name: arith.py
purpose: basic arithmetic as functions
functions: add, subtract, multiply, divide, modulus
"""

def sub(a, b):
    return a - b

$ git status -s
UU arith.py
$ git add .
$ git status -s
M  arith.py

$ git commit -m 'arith:a-minus - added subtract function'
[a-minus 710bec3] arith:a-minus - added subtract function
 1 file changed, 6 insertions(+)

$ git push -u origin a-minus
Counting objects: 3, done.
Delta compression using up to 12 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 421 bytes | 421.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
remote: 
remote: Create pull request for a-minus:
remote:   https://bitbucket.org/mktutes/demo-branch/pull-requests/new?source=a-minus&t=1
remote: 
To https://bitbucket.org/mktutes/demo-branch.git
 * [new branch]      a-minus -> a-minus
Branch 'a-minus' set up to track remote branch 'a-minus' from 'origin'.
```

### change a-plus branch

```
$ git checkout a-plus
Switched to branch 'a-plus'

$ vi arith.py 

$ git status -s
 M arith.py

$ git add .

$ git commit -m 'arith:a-plus - added add() function'
[a-plus 3458e9d] arith:a-plus - added add() function
$ git push -u origin a-plus
...
Branch 'a-plus' set up to track remote branch 'a-plus' from 'origin'.
```

### change a-prod branch

```
$ git checkout a-prod
Switched to branch 'a-prod'

$ vi arith.py 
$ git status -s
 M arith.py
$ git add .

$ git commit -m 'arith:a-prod - added multiply function'
[a-prod 31a0161] arith:a-prod - added multiply function

$ git push -u origin a-prod
```

### change a-div branch


```
Switched to branch 'a-div'
$ vi arith.py 

$ git status -s
 M arith.py
$ git add .
$ git status -s
M  arith.py

$ git commit -m 'arith:a-div - added divide function'
[a-div 72bd3c6] arith:a-div - added divide function

$ git push -u origin a-div
```

### change a-mod branch

```
$ git checkout a-mod
Switched to branch 'a-mod'

$ vi arith.py 
$ git status -s
 M arith.py
$ git add .
$ git status -s
M  arith.py

$ git commit -m 'arith:a-mod - added modulus function'
[a-mod 15925c9] arith:a-mod - added modulus function

$ git push -u origin a-mod
```

## So far

We have created 5 branches and each branch changed the `arith.py` script by adding a function. 
Now Let us merge the branches to intermediate branches. Think of this as merging changes from feature branch to dev branch 
(long-running). Later everything will be merged to `master`

1. a-add_sub:
	1. a-plus
	1. a-minus
1. a-mul_div_mod
	1. a-prod
	1. a-div
	1. a-mod

### Fast-forward merge

no extra commits; pointer update to reflect the latest file

```
$ git checkout a-add_sub
Switched to branch 'a-add_sub'

$ git merge a-plus
Updating 0b1495a..3458e9d
Fast-forward
 arith.py | 3 +++
 1 file changed, 3 insertions(+)
```

### merge commit

if git is not able to merge the changes, it will result in merge conflict. git will mark the file contents where the changes 
are and human intervention is needed to resolve the conflicts. Once the conflicts are resolved, we need to commit the changes explicitly. Since the commit happened due to the result of merge conflict, this is called a **merge commit**.

```
$ git merge a-minus
Auto-merging arith.py
CONFLICT (content): Merge conflict in arith.py
Automatic merge failed; fix conflicts and then commit the result.

# review file and make changes
$ vi arith.py 

$ git status -s
UU arith.py
$ git add .
$ git status -s
M  arith.py
$ git commit -m "arith:add_sub: merged add() and subtract()"
[a-add_sub f656b2e] arith:add_sub: merged add() and subtract()

$ git push -u origin a-add_sub
Branch 'a-add_sub' set up to track remote branch 'a-add_sub' from 'origin'.


'origin'.
```

## some more branch merges

```
$ git checkout a-mul_div_mod
Switched to branch 'a-mul_div_mod'
$ git merge a-prod
Updating 0b1495a..31a0161
Fast-forward
 arith.py | 2 ++
 1 file changed, 2 insertions(+)

$ cat arith.py 
"""
"""
def multiply(a, b):
    return a * b

$ git merge a-div
Auto-merging arith.py
CONFLICT (content): Merge conflict in arith.py
Automatic merge failed; fix conflicts and then commit the result.

$ vi arith.py 

$ git status -s
UU arith.py
$ git add .
$ git status -s
M  arith.py

$ git commit -m "arith:mul_div_mod: merged multiply() and divide()"
[a-mul_div_mod 8825ef1] arith:mul_div_mod: merged multiply() and divide()
$ git push -u origin a-mul_div_mod

$ git merge a-mod
Auto-merging arith.py
CONFLICT (content): Merge conflict in arith.py
Automatic merge failed; fix conflicts and then commit the result.

$ vi arith.py 
$ git status -s
UU arith.py
$ git add .
$ git status -s
M  arith.py

$ git commit -m "arith:mul_div_mod: merged modulus()"
[a-mul_div_mod 59215c5] arith:mul_div_mod: merged modulus()
$ git push -u origin a-mul_div_mod
Branch 'a-mul_div_mod' set up to track remote branch 'a-mul_div_mod' from 'origin'.
```

### View file from 'a-mul-div-mod'

```
$ git branch
* a-mul_div_mod
  master

$ cat arith.py 
"""
name: arith.py
purpose: adding arithmeric functions for reuse
"""

def multiply(a, b):
    return a * b

def divide(a, b):
    return a / b

def modulus(a, b):
    return a % b
```

### view contents from 'a-add_sub'

```
$ git checkout a-add_sub
Switched to branch 'a-add_sub'
Your branch is up to date with 'origin/a-add_sub'.
$ cat arith.py 
"""
name: arith.py
purpose: basic arithmetic as functions for reusability
functions: add, subtract, multiply, divide, modulus
"""

def add(a, b):
    return a + b

def sub(a, b):
    return a - b

```

## FINAL Merge: To Master

```
$ git checkout master
Switched to branch 'master'
Your branch is up to date with 'origin/master'.
$ git branch
...
...
  a-prod
* master

# file in master now
$ cat arith.py 
"""
"""

$ git merge a-mul-di - not something we can merge
$ git merge a-mul_div_mod
Updating 0b1495a..59215c5
Fast-forward
 arith.py | 11 +++++++++++
 1 file changed, 11 insertions(+)

$ cat arith.py 
"""
name: arith.py
purpose: adding arithmeric functions for reuse
"""

def multiply(a, b):
    return a * b

def divide(a, b):
    return a / b

def modulus(a, b):
    return a % b


$ git merge a-add_sub
Auto-merging arith.py
CONFLICT (content): Merge conflict in arith.py
Automatic merge failed; fix conflicts and then commit the result.
```

### How merge conflict looks in a file ?

`git` places markers like '<<<<<<< HEAD', '========', '>>>>>>> a-add_sub' to mark changes. 
We need to review the contents, make changes and commit.


```
$ cat arith.py 
"""
name: arith.py
<<<<<<< HEAD
purpose: adding arithmeric functions for reuse
"""

def multiply(a, b):
    return a * b

def divide(a, b):
    return a / b

def modulus(a, b):
    return a % b
=======
purpose: basic arithmetic as functions for reusability
functions: add, subtract, multiply, divide, modulus
"""

def add(a, b):
    return a + b

def sub(a, b):
    return a - b
>>>>>>> a-add_sub
$ vi arith.py 
$ cat arith.py 
"""
name: arith.py
purpose: adding arithmeric functions for reuse
functions: add, subtract, multiply, divide, modulus
"""

def add(a, b):
    return a + b

def sub(a, b):
    return a - b

def multiply(a, b):
    return a * b

def divide(a, b):
    return a / b

def modulus(a, b):
    return a % b

## changes made


$ git add .
$ git commit -m "arith:master - completed dev of basic arith func"
$ git push -u origin master
```

## Display branches and Delete

```
$ git branch
  a-add_sub
  a-div
  a-minus
  a-mod
  a-mul_div_mod
  a-plus
  a-prod
* master

$ git branch -d a-plus
Deleted branch a-plus (was 3458e9d).
$ git branch -d a-prod
Deleted branch a-prod (was 31a0161).
$ git branch -d a-div
Deleted branch a-div (was 72bd3c6).
$ git branch -d a-mod
Deleted branch a-mod (was 15925c9).
$ git branch -d a-minus
Deleted branch a-minus (was 710bec3).

$ git branch
  a-add_sub
  a-mul_div_mod
* master

$ git branch -d a-add_sub
Deleted branch a-add_sub (was f656b2e).
$ git branch -d a-mul_div_mod
Deleted branch a-mul_div_mod (was 59215c5).

$ git branch
* master
```

## Git commit graph

![](git_branch.png)
# The END

Git

	1. create remote repo
	2. create local repo
	3. cloning a remote 
	4. forking a remote

create local repo
	1. create a directory
	2. change to that directory
	3. run the following commands: init, status, add, commit, log, diff


Creating Local Repo

	# keep all repos
	mkdir /e/repo
	cd /e/repo

	# training repos: git, linux etc..
	mkdir trng
	cd trng

	# local repo dir
	mkdir demo-local-repo
	cd demo-local-repo

	# create a repo
	git init

	# add some files

	git status 
	git status -s

	git add <FILE NAMES>

	git status 
	git status -s

	git commit -m "COMMIT MESSAGE"	

	git log
	git log --oneline

	# from git log: take the 40 bytes or 7 bytes hex-string
	git show <HEXDIGITS from git log>
	git show b925837
	git show b925837177a1fe40be26c9023d0bd1463dec521b
	
